---
title: "1. Introduction"
image: "images/blog/intro-resize.png"
description: "This is meta description."
draft: false
---
Add some text
This is an introduction to your project.  Answer the following.

1. What made you want to do this project in particular?
2. What did you hope to learn?
3. What difficulties or challenges did you anticipate?
